'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');
var secret = 'No tengo miedo de empezar desde cero';

exports.ensureAuth = function(req, res, next) {

	if(!req.headers.authorization) {

		return res.status(403).send({message: 'se necesita la autorizacion...'});
	}

	var token = req.headers.authorization.replace(/['"]+/g, '');

	try{

		var payload = jwt.decode(token, secret);

		if(payload.exp <= moment().unix()) {

			return res.status(401).send({message: 'TOKEN EXPIRADO'});
		}

	}catch(ex){
		console.log(ex);
		return res.status(404).send({message:'TOKEN NO VALIDO'});
	}

	req.user = payload;

	next();
};
'use strict'

var mongoose = require('mongoose');

mongoose.Promise = global.Promise;

var app = require('./app');

var port = process.env.PORT || 3977;

mongoose.connect('mongodb://localhost:27017/datero', {useMongoClient: true})
			.then(() => {

				console.log("La conexion a la base de datos se ha realizado correctamente...");

				app.listen(port, () => {

					console.log("Servidor escuchando en http://localhost:"+ port);

				});
					
			})
				.catch(err => console.log(err));
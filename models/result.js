'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var resultadoSchema = Schema({
	hora: String,
	fecha: String,
	status: String,
	animal: { type: Schema.ObjectId, ref: 'Animal'}
});

module.exports = mongoose.model('Resultado', resultadoSchema);
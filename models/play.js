'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var playSchema = Schema({
	hora: String,
	fecha: String,
	status: String,
	ticket: {type: Schema.ObjectId, ref:'Ticket'},
	animal: {type: Schema.ObjectId, ref:'Animal'}
	
});

module.exports = mongoose.model('Play', playSchema);
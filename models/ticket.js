'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ticketSchema = Schema({
	serial: String,
	hora: String,
	fecha: String,
	status: String,
	user: {type: Schema.ObjectId, ref:'User'}
});

module.exports = mongoose.model('Ticket', ticketSchema);
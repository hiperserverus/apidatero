'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var animalSchema = Schema({

	name: String,
	number: String,
	image: String
});

module.exports = mongoose.model('Animal', animalSchema);
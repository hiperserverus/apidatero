'use strict'

var express = require('express');
var bodyParser = require('body-parser');

var app = express();

//REQUIRE ROUTES

var userRoutes = require('./routes/user');
var animalRoutes = require('./routes/animal');
var resultRoutes = require('./routes/result');
var ticketRoutes = require('./routes/ticket');
var playRoutes = require('./routes/play');


app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

// HEADERS HTTP

app.use((req, res, next) => {

	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Requested-Method');
	res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
	res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
	next();
});

// ROUTES BASE

app.use('/api', userRoutes);
app.use('/api', animalRoutes);
app.use('/api', resultRoutes);
app.use('/api', ticketRoutes);
app.use('/api', playRoutes);

module.exports = app;
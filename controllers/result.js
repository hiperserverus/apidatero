'use strict'

//Models

var resultModel = require('../models/result');

function addResults(req, res){
	
	var newResult = new resultModel();
	var params = req.body;

	newResult.hora = 'null';
	newResult.fecha = 'null';
	newResult.status = 'NONE';
	newResult.animal = params.animal;

	newResult.save((err, resultStored) => {

		if(err){
			res.status(500).send({message:'Error en la peticion agregar resultado'});
		}else{
			if(!resultStored) {
				res.status(404).send({message:'No se pudo guardar el resultado'});
			}else{
				res.status(200).send({resultStored});
			}
		}
	});

}

function getResult(req, res) {

	var resultId = req.params.id;

	resultModel.findById(resultId).populate({path: 'animal'}).exec((err, result) =>{

		if(err){
			res.status(500).send({message:'Error en la peticion obtener resultado'});
		}else{
			if(!result){
				res.status(200).send({message:'No se encontro ningun resultado para el id indicado'});
			}else{
				res.status(200).send({result});
			}
		}
	});
}

function getResults(req, res) {

	resultModel.find({}).populate({path: 'animal'}).exec((err, results) =>{

		if(err){
			res.status(500).send({message:'Error en la peticion obtener resultados'});
		}else{
			if(!results) {
				res.status(200).send({message:'No se encontraron resultados'});
			}else{
				res.status(200).send({results});
			}
		}
	});
}

function updateResult(req, res) {

	var resultId = req.params.id;

	var editResult = req.body;

	resultModel.findByIdAndUpdate(resultId, editResult, {new: true}, (err, resultUpdate) => {

		if(err){
			res.status(500).send({message:'Error en la peticion actualizar usuario'});
		}else{
			if(!resultUpdate){
				res.status(404).send({message: 'Error al actualizar resultado'});
			}else{
				res.status(200).send({resultUpdate});
			}
		}
	});
}

function deleteResult(req, res) {

	var resultId = req.params.id;

	resultModel.findByIdAndRemove(resultId, (err, resultDelete) => {

		if(err){
			res.status(500).send({message:'Error en la peticion eliminacion resultado'});
		}else{
			if(!resultDelete){
				res.status(404).send({message:'No se encontro el resultado indicado para eliminar'});
			}else{
				res.status(200).send({resultDelete});
			}
		}
	});
}

module.exports = {
addResults,
getResult,
getResults,
updateResult,
deleteResult
};
'use strict'

// MODULES

var bcrypt = require('../node_modules/bcrypt-nodejs');
var fs = require('fs');
var path = require('path');

//MODELS

var userModel = require('../models/user');

//SERVICES

var jwt = require('../services/jwt');



function createUser(req, res) {

	var user = new userModel();
	var params = req.body;

	user.name = params.name;
	user.surname = params.surname;
	user.email = params.email;
	user.role = 'ROLE_ADMIN';
	user.image = 'null';
	user.expiration = 'NEVER';

	if (params.password) {

		bcrypt.hash(params.password, null, null, (err, hash) => {

			user.password = hash;

			if (user.name != null && user.surname !=null && user.email != null){

				user.save((err, userStored) => {

					if(err) {
						res.status(500).send({Message: 'Error al guardar Usuario'});
					}else{

						if(!userStored) {
							res.status(500).send({Message:'No se ha registrado el usuario'});
						}else{
							res.status(200).send({user: userStored});
						}

					}
				});

			}else{
				res.status(200).send({message:'Faltan campos por rellenar...'})
			}
		});

	}else{
		res.status(200).send({message: 'Falta la contraseña...'});
	}

}

function loginUser(req, res){

	var params = req.body;

	var email = params.email;
	var password = params.password;

	userModel.findOne({email: email.toLowerCase()}, (err, user) => {

		if(err){
			res.status(500).send({message: 'Error en la petición de login...'});
		}else{
			if(!user){
				res.status(404).send({message: 'Usuario no existe..'});
			}else{
				//COMPROBAION DELA CONTRASEÑA COMPARANDO CON BCRYPT
				bcrypt.compare(password, user.password, (err, check) =>{

					if(check) {

						if(params.gethash){

							res.status(200).send({token: jwt.createToken(user)});

						}else{
							res.status(200).send({user});
						}


					}else{
						res.status(404).send({message: 'Usuario o contraseña incorrectos...'});
					}

				});
			}
		}
	});
}

function getUsers(req, res) {

	userModel.find({}, (err, users) => {

		if(err){
			res.status(500).send({message:'Error en la peticion de obtener usuarios'});
		}else{
			if(!users){
				res.status(404).send({message:'No se encontraron usuarios'});
			}else{
				res.status(200).send({users});
			}
		}
	});
}

function updateUser(req, res){

	var userId = req.params.id;
	var datosUpdate = req.body;

	if (userId != req.user.sub){
		return 	res.status(500).send({message: 'No tienes permiso para actualizar este usuario'});
	}

	userModel.findByIdAndUpdate(userId, datosUpdate, {new: true}, (err, userUpdated)=>{

		if(err){
			res.status(500).send({message:'Error en la peticion de actualización'});
		}else{
			if(!userUpdated) {
				res.status(404).send({message:'No se pudo actualizar el usuario'});
			}else{
				res.status(200).send({user: userUpdated});
			}
		}

	});

}

function deleteUser(req, res){

	var userId = req.params.id;

	userModel.findByIdAndRemove(userId, (err, userRemove) => {

		if(err){
			res.status(500).send({message:'Error en la peticion eliminar usuario'});
		}else{
			if(!userRemove){
				res.status(404).send({message:'No se pudo eliminar el usuario'});
			}else{
				res.status(200).send({userRemove});
			}
		}
	});

}

function uploadImageUser(req, res){

	var userId = req.params.userId;
	var fileName = 'none';
	var typeFiles = ['png', 'jpg', 'jpeg', 'gif'];
	var fileValid = false;

	if(!req.files) {
			res.status(200).send({message:'No se ha subido ninguna imagen'});
	}else {

			var filePath = req.files.image.path;
			var fileSplit = filePath.split('\\');
			var fileName = fileSplit[2];

			var partExtension = fileName.split('\.');
			var fileExtension = partExtension[1];

		for (var i = 0; i<typeFiles.length; i++) {

			if(typeFiles[i] == fileExtension){
				fileValid = true;
				break;
			}
		}



		if(!fileValid) {
			res.status(200).send({message:'Extension del archiva no valida'});
		}else{
			userModel.findByIdAndUpdate(userId, {image: fileName}, {new: true} , (err, userUpdated) =>{

				if (err){
					res.status(500).send({message: 'Error en la peticion de carga de avatar'});
				}else{
					if(!userUpdated) {
						res.status(404).send({message:'No se puedo subir la imagen'});
					}else{
						res.status(200).send({user: userUpdated});
					}
				}
			});
		}
		
}

}



function getUserImage(req, res) {

	var imageFile = req.params.imageFile;
	var pathFile = './uploads/users/'+ imageFile;

	fs.exists(pathFile, (exists) => {

		if (!exists) {
			res.status(200).send({message: 'imagen solicitada no existe'});
		}else{
			res.sendFile(path.resolve(pathFile));
		}
	});

}



module.exports = {
	createUser,
	loginUser,
	getUsers,
	updateUser,
	deleteUser,
	uploadImageUser,
	getUserImage
};
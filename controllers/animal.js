'use strict'

//MODULES

var fs = require('fs');
var path = require('path');

//MODELS

var userModel = require('../models/user');
var animalModel = require('../models/animal');

function createAnimal(req, res) {

	var newAnimal = new animalModel();
	var params = req.body;

	if(!params.name ) {

		res.status(200).send({message:'Existen datos faltantes'})

	}else {

		newAnimal.name = params.name;
		newAnimal.number = params.number;
		newAnimal.image = null;

		newAnimal.save((err, animalStored) => {

			if (err){
				res.status(500).send({message: 'Error en la peticion guardar animal'});
			}else{

				if (!animalStored) {
					res.status(404).send({message: 'No se puedo guardar animal'})
				}else{
					res.status(200).send({animal: animalStored});
				}
			}

		});
	}


}

function getAnimals(req, res) {

	animalModel.find({}, (err, animals) => {

		if (err) {

			res.status(500).send({message: 'Error en la peticion de listar animales'});

		}else{

			if(!animals) {

				res.status(404).send({message:'No se encontraron animales para listar'});

			}else {

				res.status(200).send({animals});
			}
		}


	});
}


function getAnimal(req, res) {

	var animalId = req.params.id;

	animalModel.findById(animalId, (err, animal) => {

		if(err) {
			res.status(500).send({message:'Error en la peticion de obtener animal'});
		}else{
			if(!animal) {
				res.status(404).send({message:'No se encontro el animal requerido'});
			}else{
				res.status(200).send({animal});
			}
		}
	});
}

function updateAnimal(req, res) {

	var animalId = req.params.id;

	var animalEdit = req.body;

	animalModel.findByIdAndUpdate(animalId, animalEdit, {new:true}, (err, animalUpdated) => {

		if(err) {
			res.status(500).send({message:'Error en la peticion de actualizar animal'});
		}else{
			if (!animalUpdated) {
				res.status(404).send({message:'No se pudo actualizar el animal especificado'});
			}else{
				res.status(200).send({animalUpdated});
			}
		}
	});
}

function uploadImageAnimal(req, res){

	var animalId = req.params.animalId;
	var fileName = 'none';
	var typeFiles = ['png', 'jpg', 'jpeg', 'gif'];
	var fileValid = false;

	if (!req.files){
		res.status(200).send({message:'No se ha subido ninguna imagen'});
	}else{

		var filePath = req.files.image.path;
		var fileSplit = filePath.split('\\');
		var fileName = fileSplit[2];

		var partExtension = fileName.split('\.');
		var fileExtension = partExtension[1];

		for (var i = 0; i<typeFiles.length; i++) {

			if(typeFiles[i] == fileExtension){
				fileValid = true;
				break;
			}
		}

		if (!fileValid){
			res.status(200).send({message:'Extension del archiva no valida'});
		}else{

			animalModel.findByIdAndUpdate(animalId, {image: fileName}, {new:true}, (err, animalUpdated) => {

				if(err){
					res.status(500).send({message:'Error en la peticion de subida de imagen animal'});

				}else{
					if(!animalUpdated){
						res.status(404).send({message:'No se pudo subir la imagen del animal'});
					}else{
						res.status(200).send({animalUpdated});
					}
				}
			});
		}


	}
}

function getAnimalImage(req, res) {

	var fileImage = req.params.imageFile;
	var filePath = './uploads/animals/'+ fileImage;

	fs.exists(filePath, (exists) => {

		if(!exists) {
			res.status(200).send({message:'Imagen solicitada no existe'});
		}else{
			res.sendFile(path.resolve(filePath));
		}
	});
}

function deleteAnimal(req, res) {

	var animalId = req.params.id;

	animalModel.findByIdAndRemove(animalId, (err, animalRemove) => {

		if(err){
			res.status(500).send({message:'Error en la peticion de borrado de animal'});
		}else{
			if(!animalRemove){
				res.status(404).send({message:'No se puedo eliminar el animal especificado'});
			}else{
				res.status(200).send({animalRemove});
			}
		}
	});

}


module.exports = {

createAnimal,
getAnimals,
getAnimal,
updateAnimal,
uploadImageAnimal,
getAnimalImage,
deleteAnimal


};
'use strict'

var ticketModel = require('../models/ticket');

function createTicket(req, res) {
	var newTicket = new ticketModel();
	var params = req.body;

	newTicket.serial = '0';
	newTicket.hora = '0:00';
	newTicket.fecha = '0/00/0000';
	newTicket.status = 'activo';
	newTicket.user = params.user;

	newTicket.save((err, ticket) => {
		if(err){
			res.status(500).send({message:'Error en la peticion crear ticket'});
		}else{
			if(!ticket){
				res.status(404).send({message:'No se pudo crear ticket'});
			}else{
				res.status(200).send({ticket});
			}
		}
	});
}

function getTickets(req, res){

	ticketModel.find({}, (err, tickets) => {

		if(err){
			res.status(500).send({message:'Error en la peticion obtener tickets'});
		}else{
			if(!tickets){
				res.status(200).send({message:'No se encontraron resultados'});
			}else{
				res.status(200).send({tickets});
			}
		}
	});
}

function getTicket(req, res) {

	var ticketId = req.params.id;

	ticketModel.findById(ticketId, (err, ticket) => {
		if(err){
			res.status(500).send({message:'Error en la peticion obtener ticket'});
		}else{
			if(!ticket){
				res.status(200).send({message:'No se encontro el ticket requerido'});
			}else{
				res.status(200).send({ticket});
			}
		}
	});

}

function updateTicket(req, res) {

	var ticketId = req.params.id;
	var editTicket = req.body;

	ticketModel.findByIdAndUpdate(ticketId, editTicket, {new: true}, (err,ticketUpdate) => {
		if(err){
			res.status(500).send({message:'Error en la peticion actualizar ticket'});
		}else{
			if(!ticketUpdate){
				res.status(404).send({message:'No se pudo actualizar el ticket'});
			}else{
				res.status(200).send({ticketUpdate});
			}
		}
	});

}

function deleteTicket(req, res) {
	var ticketId = req.params.id;

	ticketModel.findByIdAndRemove(ticketId, (err, ticketDelete) => {
		if(err){
			res.status(500).send({message:'Error en la peticion eliminar ticket'});
		}else{
			if(!ticketDelete) {
				res.status(404).send({message:'No se encontro el ticket o ya ha sido eliminado'});
			}else{
				res.status(200).send({ticketDelete});
			}
		}
	});
}

module.exports = {
	createTicket,
	getTickets,
	getTicket,
	updateTicket,
	deleteTicket
};
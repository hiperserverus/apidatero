'use strict'

var playModel = require('../models/play');

function createPlay(req, res) {

	var newPlay = new playModel();
	var params = req.body;

	newPlay.hora = '00:00';
	newPlay.fecha = '00/00/0000';
	newPlay.status = 'Activo';
	newPlay.animal = params.animal;
	newPlay.ticket = params.ticket;

	newPlay.save((err, play) => {
		if(err){
			res.status(500).send({message:'Error en la peticion crear jugada'});
		}else{
			if(!play){
				res.status(404).send({message:'No se pudo guardar la jugada'});
			}else{
				res.status(200).send({play});
			}
		}
	});
}

function getPlay(req, res) {

	var playId = req.params.id;

	playModel.findById(playId).populate({path: 'ticket'}).populate({path: 'animal'}).exec((err, play) => {
		if (err){
			res.status(500).send({message:'Error en la peticion obtener jugada'});
		}else{
			if(!play){
				res.status(404).send({message:'No se encontro la jugada requerida'});
			}else{
				res.status(200).send({play});
			}
		}
	});

}

function getPlays(req, res){

	var ticketId = req.params.ticketId;

	playModel.find({ticket: ticketId}).populate({path: 'ticket'}).populate({path: 'animal'}).exec((err, plays) => {

		if(err){
			res.status(500).send({message:'Error en la peticion obtener jugadas'});
		}else{
			if(!plays){
				res.status(404).send({message:'No se encontraron las jugadas para este ticket'});
			}else{
				res.status(200).send({plays});
			}
		}
	});
}

function editPlay(req, res) {

	var playId = req.params.id;

	var editPlay = req.body;

	playModel.findByIdAndUpdate(playId, editPlay, {new: true}, (err, playUpdated) => {
		if(err){
			res.status(500).send({message:'Error en la peticion editar jugada'});
		}else{
			if(!playUpdated){
				res.status(404).send({message:'No se pudo actualizar la jugada'});
			}else{
				res.status(200).send({playUpdated});
			}
		}
	});
}

function deletePlay(erq, res) {

	var playId = req.params.id;

	playModel.findByIdAndRemove(playId, (err, playRemove) => {
		if(err){
			res.status(500).send({message:'Error en la peticion eliminar jugada'});
		}else{
			if(!playRemove){
				res.status(404).send({message:'No se encontro la jugada a eliminar o ya ha sido eliminada anteriormente'});
			}else{
				res.status(200).send({playRemove});
			}
		}
	});
}

module.exports = {
	createPlay,
	getPlay,
	getPlays,
	editPlay,
	deletePlay
};
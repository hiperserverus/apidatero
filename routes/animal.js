'use strict'

var express = require('express');
var AnimalController = require('../controllers/animal');
var mdAuth = require('../middlewares/authenticated');
var mdAdmin = require('../middlewares/admin');

var multipart = require('connect-multiparty');
var mdUpload = multipart({ uploadDir: './uploads/animals'});

var api = express.Router();

//ROUTES ANIMALS

api.post('/animal/create', [mdAuth.ensureAuth, mdAdmin.isAdmin, mdUpload], AnimalController.createAnimal);
api.get('/animals', mdAuth.ensureAuth, AnimalController.getAnimals);
api.get('/animal/:id', mdAuth.ensureAuth, AnimalController.getAnimal);
api.post('/animal/edit/:id', mdAuth.ensureAuth, mdAdmin.isAdmin, AnimalController.updateAnimal);
api.post('/animal/upload-image/:animalId', [mdAuth.ensureAuth, mdAdmin.isAdmin, mdUpload], AnimalController.uploadImageAnimal);
api.get('/animal/image/:imageFile', mdAuth.ensureAuth, AnimalController.getAnimalImage);
api.delete('/animal/:id', mdAuth.ensureAuth, mdAdmin.isAdmin, AnimalController.deleteAnimal);

module.exports = api;

'use strict'

var express = require('express');
var UserController = require('../controllers/user');
var mdAuth = require('../middlewares/authenticated');
var mdAdmin = require('../middlewares/admin');

var multipart = require('connect-multiparty');
var mdUpload = multipart({ uploadDir: './uploads/users'});

var api = express.Router();

//ROUTES USERS

api.post('/register', UserController.createUser);
api.post('/login', UserController.loginUser);
api.get('/users', UserController.getUsers);
api.put('/user-update/:id', mdAuth.ensureAuth , UserController.updateUser);
api.delete('/user/:id', [mdAuth.ensureAuth, mdAdmin.isAdmin], UserController.deleteUser);
api.post('/upload-image/:userId', [mdAuth.ensureAuth, mdUpload], UserController.uploadImageUser);
api.get('/get-image/:imageFile', UserController.getUserImage);

module.exports = api;
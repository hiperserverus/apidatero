'use strict'

var express = require('express');
var TicketController = require('../controllers/ticket');
var mdAuth = require('../middlewares/authenticated');
var mdAdmin = require('../middlewares/admin');


var api = express.Router();

api.post('/ticket/create', TicketController.createTicket);
api.get('/tickets', TicketController.getTickets);
api.get('/ticket/:id', TicketController.getTicket);
api.put('/ticket/:id', TicketController.updateTicket);
api.delete('/ticket/:id', TicketController.deleteTicket);

module.exports = api;
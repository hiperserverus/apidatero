'use strict'

var express = require('express');
var PlayController = require('../controllers/play');
var mdAuth = require('../middlewares/authenticated');
var mdAdmin = require('../middlewares/admin');

var api = express.Router();

api.post('/play/create', PlayController.createPlay);
api.get('/ticket/play/:id', PlayController.getPlay);
api.get('/ticket/plays/:ticketId', PlayController.getPlays);
api.put('/ticket/play/:id', PlayController.editPlay);
api.delete('/ticket/play/:id', PlayController.deletePlay);

module.exports = api;


'use strict'

var express = require('express');
var ResultController = require('../controllers/result');

var mdAuth = require('../middlewares/authenticated');
var mdAdmin = require('../middlewares/admin');

var multipart = require('connect-multiparty');
var mdUpload = multipart({ uploadDir: './uploads/users'});

var api = express.Router();

api.post('/result/create', [mdAuth.ensureAuth, mdAdmin.isAdmin], ResultController.addResults);
api.get('/result/:id', mdAuth.ensureAuth, ResultController.getResult);
api.get('/results', mdAuth.ensureAuth, ResultController.getResults);
api.put('/result/:id', [mdAuth.ensureAuth, mdAdmin.isAdmin], ResultController.updateResult);
api.delete('/result/:id', [mdAuth.ensureAuth, mdAdmin.isAdmin], ResultController.deleteResult);

module.exports = api;